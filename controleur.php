<?php

require_once 'vue.php';
class Controleur
{
    private $vue;

    public function __construct()  
    {
        $this->vue = new Vue();
    }
    public function acceuil()
    {
        $this->vue->accueil();
    }
    public function aPropos()
    {
        $this->vue->aPropos();
    }

    public function getVue() {
        return $this->vue;
    }
}
