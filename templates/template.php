<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
	<link rel="shortcut icon" href="ressources/images/logo.png" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="ressources/stylevep.css" />
    <link rel="stylesheet" type="text/css" href="ressources/form_dep.css">
	<title>VEP</title>

</head>

<body>

<?php 
if ($_GET['module']=='annonces' && $_GET['action']=='liste_annonces') {
	# code...
?>
	<!-- HEADER -->
	<div class="block">
		<div class="banner">
			<div class="couche-image"></div>
			<img src="ressources/images/fond-site.jpg" alt="Un ordinateur avec du code" class="banner-image">


			<!-- <div class="image-fond">
		<div class="couche-image"></div>
	</div> -->
			<div class="page-recherche">

				<!-- HEADER -->
				<header class="header header-acceuil">
					<a href="index.php?module=annonces"><img alt="logo" src="ressources/images/logo.png" /></a>
					<nav>
						<ul>
							<li>
								<a class="couleur-survol" class="is-active" href="index.php?module=annonces&action=deposerAnnonce">Déposer une annonce</a>
								<!-- <a class="couleur-survol" class="is-active" href="index.php?module=connexion&action=form_connexion&destination='index.php?module=annonces@action=ajouterAnnonces'">Déposer une annonce</a> -->
								<a class="couleur-survol" href="index.php?module=connexion&action=form_connexion&destination='index.php?module=membre'">Espace Membre</a>
								<a class="couleur-survol" href="index.php?module=contact">Nous contacter</a>
								<a class="couleur-survol" href="index.php?module=apropos">A propos</a>
							</li>
						</ul>
					</nav>
				</header><br><br><br>

				<!-- <div class="container">
					<a class="has-text-black" href="index.php?module=inscription&action=form_inscription">form_inscr</a>
					<a class="has-text-black" href="index.php?module=connexion&action=form_connexion">Connexion</a>
					<a class="has-text-black" href="index.php?module=annonces&action=liste_annonces">Annonces</a>
				</div> -->
				<!-- PHRASE D\'ACCROCHE -->
				<h1 class="title is-1 has-text-centered has-text-white">
					Le premier vrai site de vente entre particuliers
				</h1>
				<p class="title is-4 has-text-centered has-text-white">
					Enfin des petits prix sur de bons articles
				</p>
				<!-- RECHERCHER -->
				<section class="section-recherche">
					<div class="columns recherche has-text-centered">
						<form method="POST" action="#">
							<div class="block-recherche">
								<div class="column is-6">
									<label for="mots" class="label has-text-white has-text-left is-size-4">Mots-clés</label>
									<div class="control">
										<input id="mots" class="input is-large is-medium width-input" type="text" placeholder="Rechercher...">
									</div><br><br>
								</div>
								<div class="column is-6">
									<label class="label has-text-white has-text-left is-size-4">Région</label>
									<div class="control">
										<div class="select is-fullwidth is-large is-info is-medium">
											<select class="is-hovered">
												<option>Vitry-sur-Seine</option>
												<option>Ile-de-France</option>
												<option>Seine-et-Marne</option>
												<option>Normandie</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="block-recherche">
								<div class="column is-6">
									<label class="label has-text-white has-text-left is-size-4">Catégorie</label>
									<div class="control">
										<div class="select is-fullwidth is-large is-info is-medium">
											<select class="is-hovered">
												<option>Informatique</option>
												<option>Multimédia</option>
												<option>Electroménager</option>
												<option>Téléphonie</option>
												<option>Auto</option>
												<option>Immobilier</option>
												<option>Emploi</option>
											</select>
										</div>
									</div>
								</div>
								<div class="column is-6">
									<label for="code_postal" class="label has-text-white has-text-left is-size-4">Code Postal</label>
									<div class="control">
										<input id="code_postal" class="input is-large is-medium width-input" type="text" placeholder="Code postal...">
									</div><br><br>
								</div>
							</div>
							<a class="button is-link is-size-4 has-text-white button-search">
								<span class="icon is-small">
								<i class="fas fa-search"></i>
								</span>
								<span>Rechercher</span>
							</a><br><br>
						</form>
					</div>
				</section>
			</div>
		</div>
	</div>
<?php
	}else{
		?>
			<!-- HEADER -->
	<header class="header">
		<a href="index.php?module=annonces"><img alt="logo" src="ressources/images/logo.png" /></a>
		<nav>
			<ul>
				<li>
					<a class="couleur-survol" class="is-active" href="index.php?module=annonces&action=deposerAnnonce">Déposer une annonce</a>
					<!-- <a class="couleur-survol" class="is-active" href="index.php?module=connexion&action=form_connexion&destination='index.php?module=annonces@action=ajouterAnnonces'">Déposer une annonce</a> -->
					<a class="couleur-survol" href="index.php?module=connexion&action=form_connexion&destination='index.php?module=membre'">Espace Membre</a>
					<a class="couleur-survol" href="index.php?module=contact">Nous contacter</a>
					<a class="couleur-survol" href="index.php?module=apropos">A propos</a>
				</li>
			</ul>
		</nav>
	</header><br><br><br>
	<?php
	}	

	echo $contenu ?>


	<!-- FOOTER -->
	<footer class="footer">
		<div class="content has-text-centered has-text-white ">
			<p>
				Site développé par Faudel, Amine et Habacuc.
			</p>
			<h4 class="has-text-white">
				Copyright &amp; tous droits reservés.
			</h4>
		</div>
	</footer>

	
	<script src="../ressources/index.js"></script>
</body>

</html>