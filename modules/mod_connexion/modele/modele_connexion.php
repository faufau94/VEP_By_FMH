<?php
require_once 'connexionBDD/connexionBDD.php';

class ModeleConnexion extends ConnexionBDD
{
    public function  __construct(){}
    
    public function getMail($pseudo){
        $pseudo  = htmlspecialchars($pseudo);
        $mail = self::$bdd->prepare("SELECT `mail`FROM `utilisateurs` WHERE `pseudo`=?");
        try{
            $mail->execute(array($pseudo));
            return $mail->fetch()[0];
        }catch(\Throwable $th){
            return "";
        }
    }
    public function getMdp($idConnexion){
        $idConnexion=htmlspecialchars($idConnexion);
        $mdp = self::$bdd->prepare("SELECT `mdp` FROM `utilisateurs` WHERE `mail`=?");
        try{
        $mdp->execute(array($idConnexion));
            return $mdp->fetch()[0];
        } catch (\Throwable $th) {
            return "";
        }        
    }    
    public function getId($idConnexion,$mdp){
        // rendvoi -1 en cas d'erreur sinon l'id de l'utilisateur
        // Le mot de passe reçu est en sha384
        $idConnexion=htmlspecialchars($idConnexion);
        $mdp = htmlspecialchars($mdp);
        $id = self::$bdd->prepare("SELECT `idUtilisateur` FROM `utilisateurs` WHERE `mail`=? AND `mdp`=?");
        try {
            $id->execute(array($idConnexion,$mdp));
            return (int)$id->fetch()[0];
        } catch (\Throwable $th) {
            return (int)-1;
        }
    }
}
