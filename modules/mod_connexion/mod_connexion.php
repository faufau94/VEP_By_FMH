<?php
/**
* Module Connexion
*/
require_once 'controleur/cont_connexion.php';

class ModConnexion
{

    private $contConnexion;

    public function __construct() {
        $this->contConnexion = new ContConnexion();
    }

    public function actionConnexion() {
		if(isset($_GET['action'])){
			$action = $_GET['action'];

		}else{
			$action = "form_connexion";
			$destination = "index.php?module=membre";
		}
		if (isset($_GET['destination'])) {
			$destination = $_GET['destination'];
			
		}else{
			$destination = "index.php?module=membre";
		}
		$destination  = str_replace("@","&",$destination);
		$destination  = str_replace("'","",$destination);
		
		switch ($action) {
			case 'deconnexion':
				$this->contConnexion->deconnexion();
				break;
			default:
			$this->contConnexion->form_connexion($destination);
				break;
		}
	}

	public function getControleur() {
		return $this->contConnexion;
	}
}
