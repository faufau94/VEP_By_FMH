<?php

/**
* Vue Connexion
*/

require_once 'vue_generique.php';

class VueConnexion extends VueGenerique
{
	public function afficheErreurs($erreur){
		$msg = "<div class=\"block\"><div class=\"columns is-centered\"><div class=\"column is-4 \"><div class=\"notification is-danger\"><button class=\"delete\"></button>$erreur</div></div></div></div>";
        echo $msg;
    }
	public function afficheFormulaire(){
        ?>
		<div class="block">
            <div class="container">
                <h1 class="title is-1 has-text-centered">Connexion</h1>
            </div>
        </div>
        <form class="container" method="POST" action="">
			<div class="columns is-centered">
				<div class="column is-4 ">

					<div class="field">
						<div class="control has-icons-left">
							<input class="input" type="text" name="idConnexion" placeholder="Identifiant">

							<span class="icon is-small is-left">
								<i class="fas fa-id-card-alt"></i>
							</span>
						</div>
					</div>

					<div class="field">
						<div class="control has-icons-left">
							<input class="input" type="password" name="mdp" placeholder="Mot de passe">
							<span class="icon is-small is-left">
								<i class="fas fa-key"></i>
							</span>
						</div>
					</div>

					<div class="field">
						<div class="control">
							<label class="label" for="connexion">
								<input type="checkbox" id="connexion" name="resterCo">
								Rester connecté
							</label>
						</div>
					</div>

					<div class="buttons has-addons is-centered">
						<button class="button is-link">Connexion</button>
					</div>

					<div class="field is-right">
						<div class="control has-text-centered">
							<p>Vous n'avez pas de compte ?</p>
							<a href="?module=inscription&action=form_inscription">Je m'inscris !</a>
						</div>
					</div>
					
				</div>
			</div>
		</form>
        <?php
    }
}