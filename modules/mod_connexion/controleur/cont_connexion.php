<?php
require_once 'modules/mod_connexion/modele/modele_connexion.php';
require_once 'modules/mod_connexion/vue/vue_connexion.php';

class ContConnexion {
    private $modele_connexion;
    private $vue_connexion;
    private $erreurs;

	public function __construct() {
		$this->modele_connexion = new ModeleConnexion();
        $this->vue_connexion = new VueConnexion();
        $this->erreurs = array();
    }
    
    public function form_connexion($destination)
    {
        if (!empty($_SESSION['idConnexion'])) {//Déja connecté
            header("Location: $destination");
        }
        if (!empty($_POST['idConnexion']) && !empty($_POST['mdp'])) {//formulaire complet
            $idConnexion = htmlspecialchars($_POST['idConnexion']);
            $mdpEntree = htmlspecialchars($_POST["mdp"]);
            $this->connexion($idConnexion,$mdpEntree,$destination);
        }else{//formulaire incomplet
            $this->erreurs="Veuillez remplir tout le formulaire !!!";
        }
        /*********** ERREURS + FORM ***********/
        if(isset($_POST['idConnexion'])){
            $this->vue_connexion->afficheErreurs($this->erreurs);
        }
        
        $this->vue_connexion->afficheFormulaire();
    }
    
    public function connexion($idConnexion, $mdp,$destination) {
        /* 
        le mdp doit etre envoyé non hashé
        la fonction retourne un tableau d'erreur (vide si deja connecté ou connexion OK) 
        */
        
        
        /*********** VERIFICATION DES DONNEES ***********/
        if(!$this->isMail($idConnexion)){
            if($this->isPseudo($idConnexion)){
                $idConnexion = $this->modele_connexion->getMail($idConnexion);
            }else{
                //c'est pas une adresse mail ni un pseudo.
                $this->erreurs="Veuilez entrez un identifiant de connexion valide (mail ou pseudo) !!!";
                return $this->erreurs;
            }
        }

        /*********** SECURISATION DES DONNEES ***********/
        $mdp = htmlspecialchars($mdp);
        $idConnexion = htmlspecialchars($idConnexion);
        /*********** HASHAGE DU MOT DE PASSE ***********/
        $mdp=hash ("sha384", $mdp);
        /**** RECUPERATION DU MOT DE PASSE EN SHA384 DANS LA BASE DE DONNÉES ****/ 
        $mdpBDD=$this->modele_connexion ->getMdp($idConnexion);
        if($mdpBDD==null) {//id de connexion invalide
            $this->erreurs="Votre identifiant de connexion est invalide !!!";
            return $this->erreurs;
        }
        // comparaison des mots de passe
        if(strcmp($mdp, $mdpBDD)==0) {
            $_SESSION['idConnexion']=$this->modele_connexion->getId($idConnexion, $mdp);
            header("Location: $destination");
        }else {
            $this->erreurs="Vous vous êtes trompés de mot de passe !!!";
            return $this->erreurs;
        }

    }

    public function deconnexion()
    {
        $_SESSION = array();
        session_destroy();
        header('Location: index.php');
    }
    private function isMail($mail){
        // vrais si c'est une adresse mail
        return (boolean)filter_var($mail, FILTER_VALIDATE_EMAIL);
    }
    private function isPseudo($pseudo){
        // vrai si c'est un pseudo (longeur: max 15, min 4/commence par une lettre/pas d'accents et caractère spéciaux)
        return (boolean)preg_match("%^([a-zA-Z][[:alnum:]]{3,14})$%",$pseudo);
    }
    public function getVue() {
        return $this->vue_connexion;
    }
}