 <?php

/**
* Vue Inscription
*/

require_once 'vue_generique.php';

class VueInscription extends VueGenerique
{
    function afficheErreur($erreurs) {
        
            ?> 
            <div class='block'> 
                <div class='columns is-centered'>
                    <div class='column is-4f'>
                        <div class='notification is-danger'>
                            <button onclick="suppNotif();" class='delete bouttonSupp'></button>
                            <ul>
                                <?php foreach ($erreurs as $value) {
                                    echo "<li>$value</li>";
                                }
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        
    }

    function form_inscription()
    {
       ?>
       <div class="block">
        <section class="block">
                <div class="block">
                <div class="container">
                    <h1 class="title is-1 has-text-centered">Inscription</h1>
                </div>
            </div>
                <form class="container" method="POST" action="index.php?module=inscription&action=envoieDonnees">
                
            <div class="columns is-centered">
                <div class="column is-6 ">
                <div class="field is-horizontal">
                    <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded has-icons-left">
                        <input class="input" type="text" placeholder="Nom" name="nom" required>
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                        </p>
                    </div>
                    <div class="field">
                        <div class="control is-expanded has-icons-left">
                            <input class="input" type="text" placeholder="Prénom" name="prenom" required>
                            
                            <span class="icon is-small is-left">
                                <i class="fas fa-user"></i>
                            </span>
                        </div>
                    </div>
                    </div>
                </div>

                    <div class="field is-horizontal">
                    <div class="field-body">
                    <div class="field">
                        <div class="control is-expended has-icons-left">
                            <input class="input" type="text" placeholder="Pseudo" name="pseudo" required>
                            
                            <span class="icon is-small is-left">
                                <i class="fas fa-user"></i>
                            </span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="control is-expended has-icons-left">
                            <input class="input" type="email" placeholder="Mail" name="mail" required>
                            
                            <span class="icon is-small is-left">
                                <i class="fas fa-envelope"></i>
                            </span>
                        </div>
                    </div>
                    </div>     
                </div>

                <div class="field is-horizontal">
                    <div class="field-body">
                    <div class="field">
                        <div class="control is-expended has-icons-left">
                        <input class="input" type="password" name="mdp" placeholder="Mot de passe" required>
                        <span class="icon is-small is-left">
                            <i class="fas fa-key"></i>
                        </span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="control is-expended has-icons-left">
                        <input class="input" type="password" name="mdp2" placeholder="Vérification mot de passe" required>
                        <span class="icon is-small is-left">
                            <i class="fas fa-key"></i>
                        </span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="field">
                    <div class="control has-icons-left">
                        <input class="input" type="text" name="adresse" placeholder="Adresse" required>
                        
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                </div>
                <div class="field">
                    <div class="control has-icons-left">
                    <input class="input" type="number" name="codePostal" placeholder="Code postal" required>
                    <span class="icon is-small is-left">
                        <i class="fas fa-key"></i>
                    </span>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-body">
                    <div class="field">
                        <div class="control is-expanded has-icons-left">
                        <input class="input" type="tel" name="telephone" placeholder="Téléphone" required>
                        <span class="icon is-small is-left">
                            <i class="fas fa-key"></i>
                        </span>
                        </div>
                    </div>
                    <div class="field">
                    <div class="control is-expanded has-icons-left">
                        <div class="select is-grey">
                        <select name="paymentFav" required>
                            <option value="" disabled selected>Choisir mode de paiement favoris</option>
                            <option>Carte bleu - CB</option>
                            <option>Chèque </option>
                            <option>Espèces </option>
                        </select>
                        </div>
                        <span class="icon is-small is-left">
                            <i class="fas fa-key"></i>
                        </span>
                    </div>
                    </div>
                    </div>
                </div>
                            <div class="buttons has-addons is-centered">
                                <button class="button is-link " name="bouton_inscription">S'inscrire</button>
                            </div>
                            <div class="field is-right">
                                <div class="control has-text-centered">
                                    <p>Déjà inscrit ?</p>
                                    <a href="index.php?module=connexion&action=form_connexion">Je me connecte !</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </section>
            </div>
            
        <?php 
    }
}