<?php

require_once 'connexionBDD/connexionBDD.php';

class ModeleInscription extends ConnexionBDD
{
    function insererDonnees($tabDonnees) {
        $envoieDonnees = parent::$bdd->prepare('INSERT INTO utilisateurs(nom, prenom, pseudo, mail, mdp, adresse, codePostal, telephone, statutcompte, paymentFav) 
                                                VALUES(?,?,?,?,?,?,?,?,?,?)');
        $envoieDonnees->execute(array(
            $tabDonnees['nom'],
            $tabDonnees['prenom'],
            $tabDonnees['pseudo'],
            $tabDonnees['mail'],
            $tabDonnees['mdp'],
            $tabDonnees['adresse'],
            $tabDonnees['codePostal'],
            $tabDonnees['telephone'],
            TRUE,
            $tabDonnees['paymentFav']));
    }

    public function verifDonnee($champ, $post)
    {
        $req = self::$bdd->prepare("SELECT $champ FROM utilisateurs WHERE $champ = :pseudo");
        $req->bindParam(':pseudo',$post,PDO::PARAM_STR);
        $req->execute();
        return $req->rowCount();
    }
}