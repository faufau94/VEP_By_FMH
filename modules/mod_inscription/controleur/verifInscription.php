<?php

require_once 'vue_generique.php';

class VerifInscription {

    private $msgErreur = [];

    function getMsgErreur() { return $this->msgErreur; }

    function verifDonneeExiste($nbr_lignes_pseudo, $nbr_lignes_email) {

        if ( $nbr_lignes_pseudo > 0 )
        {
            array_push($this->msgErreur,"Pseudo déjà utilisé, Veuillez en choisir un autre.");
        }
        if ($nbr_lignes_email > 0) 
        {
            array_push($this->msgErreur,"Email déjà utilisé, veuillez en utiliser un autre");
        }
        return empty($this->getMsgErreur());
    }

    function verifFormatDonnee($tabDonnees) {

        if (!filter_var($tabDonnees['mail'], FILTER_VALIDATE_EMAIL)) {

            array_push($this->msgErreur,"Adresse Email non valide");
        }

        if ( strcmp($tabDonnees['mdp'], $tabDonnees['mdp2']) != 0 )
        {
            array_push($this->msgErreur,"Confirmation de mot de passé érronée, veuillez vérifier vos entrées");
        }

        if(!preg_match("/[0-9]{10}/", $tabDonnees['telephone'])) {

            array_push($this->msgErreur,"Numéro de téléphone invalide");
        }

        if ( !preg_match ( "#^([0-9]){5}$#" , $tabDonnees['codePostal'] ) ) 
        {
            array_push($this->msgErreur,"Code Postal invalide");
        }

         if ( !preg_match ("/^([a-zA-Z' ]+)$/" , $tabDonnees['nom'] ) ) 
        {
            array_push($this->msgErreur,"Le nom est invalide");
        }

        if ( !preg_match ("/[a-zA-Z]/", $tabDonnees['prenom'] ) )
        {
            array_push($this->msgErreur,"Le prénom est invalide");
        } 
        return empty($this->getMsgErreur());
    }
}