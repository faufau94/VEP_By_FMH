<?php

require_once 'modules/mod_inscription/modele/modele_inscription.php';
require_once 'modules/mod_inscription/vue/vue_inscription.php';
require_once 'modules/mod_inscription/controleur/verifInscription.php';

class ContInscription
{
    private $vueInscription;
    private $modeleInscription;
    private $verifInscription;

    function __construct()
    {
        $this->vueInscription    = new VueInscription();
        $this->modeleInscription = new ModeleInscription();
        $this->verifInscription  = new VerifInscription();
    }

    function form_inscription()
    {
        $this->vueInscription->form_inscription();     
    }

    function envoieDonnees() { 

        if (isset($_POST['bouton_inscription'])) {

            $tabDonnees = [
                'nom' => $_POST['nom'],
                'prenom' => $_POST['prenom'],
                'pseudo' => htmlspecialchars($_POST['pseudo']),
                'mail' => $_POST['mail'],
                'mdp' => $_POST['mdp'],
                'mdp2' => $_POST['mdp2'],
                'paymentFav' => $_POST['paymentFav'],
                'codePostal' => $_POST['codePostal'],
                'adresse' => htmlspecialchars($_POST['adresse']),
                'telephone' => $_POST['telephone']
            ];
       
            $nombre_lignes_pseudo = $this->modeleInscription->verifDonnee('pseudo', $_POST['pseudo']);
            $nombre_lignes_email = $this->modeleInscription->verifDonnee('mail', $_POST['mail']);
            
            if ( $this->verifInscription->verifDonneeExiste($nombre_lignes_pseudo, $nombre_lignes_email) && $this->verifInscription->verifFormatDonnee($tabDonnees) ) {
                $mdpCouper = hash('sha512', $_POST['mdp']);
                $mdp = substr($mdpCouper,40);
                $tabDonnees['mdp'] = $mdp;
                $this->modeleInscription->insererDonnees($tabDonnees);
                header("Location: ?");         
            } else {
                $this->vueInscription->afficheErreur($this->verifInscription->getMsgErreur());
                $this->form_inscription();
            }
        }
    }
    
    function getVue() { return $this->vueInscription; }
}