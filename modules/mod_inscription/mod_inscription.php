<?php

/*
* Module Inscription
*/

require_once 'controleur/cont_inscription.php';

class ModInscription
{
	private $contInscription;

	function __construct()
	{
		$this->contInscription = new ContInscription();
	}

	function actionInscription() {
		
		$action = $_GET['action'];

		switch ($action) {

			case 'form_inscription':
				$this->contInscription->form_inscription();
				break;

			case 'envoieDonnees':
				$this->contInscription->envoieDonnees();
				break;

			default:
				break;
		}
	}

	public function getControleur() {
		return $this->contInscription;
	}
}