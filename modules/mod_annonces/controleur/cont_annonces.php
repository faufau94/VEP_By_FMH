<?php

require_once "modules/mod_annonces/modele/modele_annonces.php";
require_once "modules/mod_annonces/vue/vue_annonces.php";

class ContAnnonces {
    private $modele_annonces;
	private $vue_annonces;

	public function __construct() {
		$this->modele_annonces = new ModeleAnnonces();
		$this->vue_annonces = new VueAnnonces();
    }

  public function listesAnnonces() {
    
    if (isset($_GET['page']) && !empty($_GET['page']) && $_GET['page'] > 0) {
      $pageActuelle = intval($_GET['page']);
    }else {
      $pageActuelle = 1;
    } 
    
    $nbAnnoncesParPages = 15;
    $positionAnnonce  = ($pageActuelle-1)*$nbAnnoncesParPages;
    
    $listesAnnonces = $this->modele_annonces->selectionAnnonces($positionAnnonce,$nbAnnoncesParPages);
    
    
    $nbAnnoncesTotales = $this->modele_annonces->selectionAnnoncesTotales();
    $nbPagesTotales = ceil($nbAnnoncesTotales/$nbAnnoncesParPages);
    
    $tabVarPagination = ["pageActuelle"       => $pageActuelle,
                         "nbPagesTotales"     => $nbPagesTotales];
    $this->vue_annonces->afficherAnnonces($listesAnnonces,$tabVarPagination);
  }

  public function getVue() {
      return $this->vue_annonces;
  }

  public function formAnnonce()
  {
    $this->vue_annonces->formAnnonce();
  }

  public function envoieDonneesAnnonce()
  {
    $this->modele_annonces->envoieDonneesAnnonce();
  }
  public function deposerAnnonce()
  {
    $this->vue_annonces->deposerAnnonce();
  }
}