<?php

/**
* Vue Annonces
*/

require_once 'vue_generique.php';

class VueAnnonces extends VueGenerique
{   

    function deposerAnnonce()
    {
    	?>
		<div class="section">
			<div class="container">

			<h1 class="title has-text-info has-text-centered">
					Déposer une annonce
				</h1>
			<br><br>

			<article class="message is-info is-medium"><div class="message-header">
				<p>Dites-nous tout</p>
				</div>
				<div class="message-body">
				<div class="columns">
					<div class="column">
					<label class="label">Titre de votre annonce</label>
					<div class="field has-addons has-addons-left">
						<div class="control">
						<input class="input" placeholder="Entrez votre titre" required="" type="text"></div>
					</div>
					</div>

					<div class="column">
					<div class="field">
						<label class="label">Catégorie</label>
						<div class="control">
						<div class="select">
							<select><option value="" disabled="disabled" selected="selected">Choisissez une catégorie</option><option>With options</option></select></div>
						</div>
					</div>
					</div>

					<div class="column">
					<label class="label">Prix</label>
					<div class="field has-addons has-addons-left">
						<div class="control has-icons-right">
						<input class="input" placeholder="Entrez le prix" required="" type="text"><span class="icon is-small is-right">
							<i class="fas fa-euro-sign"></i>
						</span>
						</div>
					</div>
					</div>
				</div>

				<div class="field">
					<label class="label">Description de votre annonce</label>           
					<div class="control">
						<textarea class="textarea" placeholder="Description" rows="8"></textarea>
					</div>
				</div>
				</div>
			</article><br><article class="message is-info is-medium"><div class="message-header">
				<p>Ajouter des photos</p>
				</div>
				<div class="message-body">
				<div class="columns">
					<div class="column is-4">
					<div class="card">
						<header class="card-header"><p class="card-header-title is-centered">
							Image 1
						</p>

						</header>
						<div class="card-content">
						<div class="field">
							<div class="file is-centered is-boxed is-white">
							<label class="file-label">
								<input class="file-input" name="resume" type="file"><span class="file-cta">
								<span class="file-icon has-text-info">
									<i class="fas fa-upload"></i>
								</span>
								<span class="file-label has-text-info">
									Ajouter une photo
								</span>
								</span>
							</label>
							</div>
						</div>
						</div>
					</div>
					</div>

					<div class="column is-4">
					<div class="card">
						<header class="card-header"><p class="card-header-title is-centered">
							Image 2
						</p>

						</header><div class="card-content">
						<div class="field">
							<div class="file is-centered is-boxed is-white">
							<label class="file-label">
								<input class="file-input" name="resume" type="file"><span class="file-cta">
								<span class="file-icon has-text-info">
									<i class="fas fa-upload"></i>
								</span>
								<span class="file-label has-text-info">
									Ajouter une photo
								</span>
								</span>
							</label>
							</div>
						</div>
						</div>
					</div>
					</div>

					<div class="column is-4">
					<div class="card">
						<header class="card-header"><p class="card-header-title is-centered">
							Image 3
						</p>

						</header><div class="card-content">
						<div class="field">
							<div class="file is-centered is-boxed is-white">
							<label class="file-label">
								<input class="file-input" name="resume" type="file"><span class="file-cta">
								<span class="file-icon has-text-info">
									<i class="fas fa-upload"></i>
								</span>
								<span class="file-label has-text-info">
									Ajouter une photo
								</span>
								</span>
							</label>
							</div>
						</div>
						</div>
					</div>
					</div>
				</div>

				</div>
			</article>
			<div class="field is-widescreen">
				<div class="control has-text-centered">
				<label class="checkbox">
					<input required="" type="checkbox">
					J'accepte les <a href="#">conditions générales de ventes</a>
				</label>
				</div>
			</div><br>

			<div class="field has-text-centered">
				<button class="button is-info is-size-3">Déposer l'annonce</button>
			</div>


			</div>
		</div>
		</div>
		<?php
    }

    function afficherAnnonces($listesAnnonces,$tabVarPagination) 
    {

    	?>

	<!-- ANNONCES -->
	<section class="section has-background-light">
		<!-- TRIER RECHERCHE ANNONCES -->
		<div class="container is-fluid columns is-pulled-right">
			<div class="column ">
				<h2 class="has-text-link">Trier par</h2>
				<div class="select">
					<select>
					<option>Pertinence</option>
					<option>Date : les plus récents</option>
					<option>Date : les moins récents</option>
					<option>Prix : les plus chers</option>
					<option>Prix : les moins chers</option>
					</select>
				</div>
			</div>
		</div>

		<!-- AFFICHAGE DES ANNONCES -->
		<div class="container largeur-container-annonces">
		<?php	for ($i=1; $i <= 15; $i++) { ?>
		 <?php   while ($donnees = $listesAnnonces->fetch()) {
		 		$desc = (strlen($donnees["description"]) > 70) ? substr($donnees["description"], 0, 80) . " ..." : $donnees["description"];

		 ?>
		<div class="columns">
			<div class="column">
				<div class="card">
					<div class="card-image">
						<a href="index.php?module=annonces&action=annonceComplete">
							<figure class="image is-1by1">
								<img src=<?= $donnees["img1"] ?> alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media ">
								<div class="media-left">
									<figure class="image is-48x48">
										<a href="index.php?module=annonces&action=vendeur"><img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image"></a>
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4"><?= $donnees["titre"] ?></p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p class="desc"><?= $desc ?></p>
								<div class="media">
									<div class="media-left">
										<time>Posté le : <?= date("d/m/Y",strtotime($donnees['dateAnn']));  ?></time>
									</div>
									<div class="media-right">
										<p class="title is-size-3 media-right  has-text-link"><?= $donnees['prix'] ?> €</p>
									</div>
								</div>
							</div>
						</div>
						</a>
					</div>
				</div>
			</div>
	<?php } } ?>
	</div>
	</section>

		<!-- PAGINATION -->
		<nav class="pagination is-rounded is-centered container is-fluid" role="navigation">
		<?php if ($tabVarPagination['pageActuelle'] != 1) { ?>

			<a class="pagination-previous" href="index.php?module=annonces&action=liste_annonces&page=<?=($tabVarPagination['pageActuelle']-1)?>">Page précédente</a>
		
		<?php }
			
			if ($tabVarPagination['pageActuelle'] != $tabVarPagination['nbPagesTotales']) { ?>
				<a class="pagination-next" href="index.php?module=annonces&action=liste_annonces&page=<?=($tabVarPagination['pageActuelle']+1)?>">Page suivante</a>
			
		<?php } ?>
		<ul class="pagination-list">
		 <?php
			for ($i=1; $i <= $tabVarPagination['nbPagesTotales']; $i++) { 
				if ($i == $tabVarPagination['pageActuelle']) { ?>
					<li><a class="pagination-link is-current" href="index.php?module=annonces&action=liste_annonces&page=<?= $i ?>"><?= $i ?></a></li> 
				<?php  } else { ?>
					<li><a class="pagination-link" href="index.php?module=annonces&action=liste_annonces&page=<?= $i ?>"><?= $i ?></a></li>

				<?php } ?>

			<!-- <li><span class="pagination-ellipsis">&hellip;</span></li> -->
				<?php } ?>
			  
		  </ul>
		</nav>

	<?php
    }
}