<?php

/**
* Module Annonces
*/

require_once 'controleur/cont_annonces.php';

class ModAnnonces
{
	private $contAnnonces;

	function __construct()
	{
		$this->contAnnonces = new ContAnnonces();
	}

	function actionAnnonces() {
		if (!isset($_GET['action'])) {
			$_GET['action'] = 'liste_annonces';
			$action = $_GET['action'];
		} else {
			$action = $_GET['action'];
		}
		switch ($action) {
			case 'liste_annonces':
				$this->contAnnonces->listesAnnonces();
				break;
			case 'form_annonce':
				$this->contAnnonces->formAnnonce();
				break;
			case 'deposerAnnonce':
				$this->contAnnonces->deposerAnnonce();
				break;
		}
		
	}

	public function getControleur() {
		return $this->contAnnonces;
	}
}