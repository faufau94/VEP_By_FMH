<?php

require_once 'connexionBDD/connexionBDD.php';

class ModeleAnnonces extends ConnexionBDD
{
    /**
     * Selection d'un nombre limité d'annonces de la plus récente à la plus 
     * ancienne.
     */
    public function selectionAnnonces($positionAnnonce,$nbAnnoncesParPages)
    {
        $reqAnnonces = self::$bdd->query("SELECT titre,prix,description,dateAnn,img1 FROM annonces ORDER BY idAnnonce LIMIT $positionAnnonce,$nbAnnoncesParPages");
    	return $reqAnnonces;
    }
    
    /**
     * Selection de toutes les annonces de la plus récente à la plus ancienne.
     */
    public function selectionAnnoncesTotales()
    {
        $reqAnnonce = self::$bdd->query('SELECT idAnnonce FROM annonces');
        return $reqAnnonce->rowCount();
    }

    /**
     * Selection de toutes les annonces de la plus récente à la plus ancienne en 
     * fonction de la recherche de l'utilisateur.
     */
    public function selectionAnnoncesFiltre()
    {
        
    }
}
