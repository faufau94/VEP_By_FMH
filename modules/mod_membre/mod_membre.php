<?php
/**
* Module Membre
*/
require_once 'controleur/cont_membre.php';

class ModMembre
{
	private $contMembre;
	
    public function __construct() {
		if(empty($_SESSION['idConnexion'])){
			header('Location: index.php');
		}
        $this->contMembre = new ContMembre();
    }

    public function actionMembre() {
		if(isset($_GET['action'])){
			$action = $_GET['action'];
		}else{
			$action = "acceuil";
		}
	
		switch ($action){
			case 'parametres':
			$this->contMembre->$action();
			break;
			default:
			$this->contMembre->acceuil();
			break;
		}
	}
	public function getControleur() {
		return $this->contMembre;
	}
}
