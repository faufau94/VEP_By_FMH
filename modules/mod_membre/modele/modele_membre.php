<?php
require_once 'connexionBDD/connexionBDD.php';

class ModeleMembre extends ConnexionBDD
{
    public function  __construct(){}
    
    public function getInfoUser($idUser){
        $req = self::$bdd->prepare("SELECT * FROM utilisateurs where idUtilisateur = ?");
        try {
            $req->execute(array($idUser));
            return $req->fetch();
        } catch (\Throwable $th) {
            return null;
        }
    }
}
