<?php

/**
* Vue Membre
*/

require_once 'vue_generique.php';

class VueMembre extends VueGenerique
{
	public function __construct(){}

	public function afficheMenu(){
		?>
		<ul>
            <li><a href="index.php?module=connexion&destination='index.php?module=membre@action=profil'">Profil</a></li>
            <li><a href="index.php?module=connexion&destination='index.php?module=membre@action=favories'">Annonces favorites</a></li>
            <li><a href="index.php?module=connexion&destination='index.php?module=membre@action=depo'">Annonces déposer</a></li>
            <li><a href="index.php?module=connexion&destination='index.php?module=membre@action=parametres'">Parametres</a></li>
        </ul>
		<?php
	}
}