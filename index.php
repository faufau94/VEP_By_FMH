<?php
session_start();
require_once 'connexionBDD/connexionBDD.php';
require_once 'controleur.php';

ConnexionBDD::initConnexion();

$controleur = new Controleur();

if (!isset($_GET['module'])) {
	$module = 'annonces';
} else {
	$module = $_GET['module'];
}


$nomModule = "Mod". ucfirst($module);

$nomAction = "action". ucfirst($module);

switch ($module) {
	
	case 'annonces':
	case 'connexion':
	case 'membre':
	case 'inscription':
		require_once "modules/mod_". $module . "/mod_" . $module . ".php";
		$initModule = new $nomModule();
		$initModule->$nomAction();

		$contenu = $initModule->getControleur()->getVue()->getAffichage();
		break;
	case 'apropos':
		$controleur->aPropos();
		$contenu = $controleur->getVue()->getAffichage();
		break;
}

include_once "templates/template.php";

