<?php

require_once 'vue_generique.php';

class Vue extends VueGenerique
{
	public function aPropos()
	{
		?>
		<section class="section container">
		<p class="title is-desktop is-3 has-text-centered ">Qui sommes-nous ?</p>
        <div class="columns features">
            <div class="column is-4 modal-button" data-target="modal-card1">
                <div class="card is-shady">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="ressources/images/ours.jpg" alt="Placeholder image">
                        </figure>
                    </div>
                        <div class="block">
							<p class="title is-3 has-text-centered">Amine</p>
                        </div><br>
                </div>
            </div>
            <div class="column is-4 modal-button" data-target="modal-card2">
                <div class="card is-shady">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="ressources/images/leopard.jpg" alt="Placeholder image">
                        </figure>
                    </div>
                        <div class="block">
							<p class="title is-3 has-text-centered">Faudel</p>
                        </div><br>
                </div>
            </div>
            <div class="column is-4 modal-button" data-target="modal-card3">
                <div class="card is-shady">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="ressources/images/rhino.jpg" alt="Placeholder image">
                        </figure>
                    </div>
                        <div class="block">
							<p class="title is-3 has-text-centered">Habacuc</p>
                        </div><br>
                </div>
            </div>
        </div>
    </section>

    <!--  ===============
    HERE BE MODALS
    ===============  -->
    <!-- 3dFlipVertical card tiny -->
    <div id="modal-card1" class="modal modal-fx-3dSlit">
        <div class="modal-background"></div>
        <div class="modal-content is-tiny">
            <!-- content -->
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="ressources/images/ours.jpg" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="ressources/images/ours.jpg" alt="linda barret avatar">
                            </figure>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">Amine</p>
                            <p class="subtitle is-6">@aaamine</p>
                        </div>
                    </div>
                    <div class="content">
                        <p>Salut moi c'est Amine et j'suis gentil. Passioné par le backend, j'aime dévelloper en PHP (surtout les vérif pour le formulaire d'inscription j'kifff ca). Bref j'suis fiancé avec une algérienne </p>
                    </div>
                </div>
            </div>
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
    </div>
    <div id="modal-card2" class="modal modal-fx-3dSlit">
        <div class="modal-background"></div>
        <div class="modal-content is-tiny">
            <!-- content -->
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="ressources/images/leopard.jpg" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="ressources/images/leopard.jpg" alt="linda barret avatar">
                            </figure>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">Faudel</p>
                            <p class="subtitle is-6">@faufau</p>
                        </div>
                    </div>
                    <div class="content">
                        <p>Bonjour, Moi c'est Faudel. Mais tu peux m'appeler Faufau ;) Je suis développeur fullstack. Je travaille chez Apple en tant que web dev fullstack. Je suis le papa de 5 enfants et je les aime plus que tout !! </p>
                    </div>
                </div>
            </div>
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
    </div>
    <div id="modal-card3" class="modal modal-fx-3dSlit">
        <div class="modal-background"></div>
        <div class="modal-content is-tiny">
            <!-- content -->
            <div class="card">
                <div class="card-image">
                    <figure class="image is-4by3">
                        <img src="ressources/images/rhino.jpg" alt="Placeholder image">
                    </figure>
                </div>
                <div class="card-content">
                    <div class="media">
                        <div class="media-left">
                            <figure class="image is-48x48">
                                <img src="ressources/images/rhino.jpg" alt="linda barret avatar">
                            </figure>
                        </div>
                        <div class="media-content">
                            <p class="title is-4">Habacuc</p>
                            <p class="subtitle is-6">@Habac</p>
                        </div>
                    </div>
                    <div class="content">
                        <p>Bonjour, moi c'est Habacuc. Je suis développeur web et j'aime mon métier. Actuellement je travaille chez Amazon en tant que front-end développeur. Je suis père de 3 enfants dont 1 qui vient d'avoir son premier enfant.</p>
                    </div>
                </div>
            </div>
        </div>
        <button class="modal-close is-large" aria-label="close"></button>
    </div>
   
	<script src="https://unpkg.com/bulma-modal-fx/dist/js/modal-fx.min.js"></script>
	 <?php
	}


    public function acceuil()
    {
		?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
	<link href="ressources/stylevep.css" rel="stylesheet" />
	<link rel="shortcut icon" href="ressources/images/logo.png" type="image/x-icon" />
	<title>VEP</title>

</head>

<body>
	<div class="block">
		<div class="banner">
			<div class="couche-image"></div>
			<img src="ressources/images/fond-site.jpg" alt="Un ordinateur avec du code" class="banner-image">


			<!-- <div class="image-fond">
		<div class="couche-image"></div>
	</div> -->
			<div class="page-recherche">

				<!-- HEADER -->
				<header class="header-acceuil">
					<img alt="logo" src="ressources/images/logo.png" />
					<nav>
						<ul>
							<li>
								<a class="is-active" href="#">Déposer une annonce</a>
								<a href="index.php?module=connexion&action=form_connexion">Espace Membre</a>
								<a href="index.php?module=inscription&action=form_inscription">Nous contacter</a>
								<a href="#">A propos</a>
							</li>
						</ul>
					</nav>
				</header><br><br><br>

				<!-- <div class="container">
					<a class="has-text-black" href="index.php?module=inscription&action=form_inscription">form_inscr</a>
					<a class="has-text-black" href="index.php?module=connexion&action=form_connexion">Connexion</a>
					<a class="has-text-black" href="index.php?module=annonces&action=liste_annonces">Annonces</a>
				</div> -->
				<!-- PHRASE D\'ACCROCHE -->
				<h1 class="title is-1 has-text-centered has-text-white">
					Le premier vrai site de vente entre particuliers
				</h1>
				<p class="title is-4 has-text-centered has-text-white">
					Enfin des petits prix sur de bons articles
				</p>
				<!-- RECHERCHER -->
				<section class="section-recherche">
					<div class="container recherche has-text-centered">
						<form method="POST" action="#">
							<div class="block-recherche">
								<div class="field">
									<label for="mots" class="label has-text-white">Mots-clés</label>
									<div class="control">
										<input id="mots" class="input is-medium width-input" type="text" placeholder="Rechercher...">
									</div><br><br>
								</div>
								<div class="field">
									<label class="label has-text-white">Région</label>
									<div class="control">
										<div class="select is-info is-medium">
											<select class="is-focused">
												<option>Vitry-sur-Seine</option>
												<option>Ile-de-France</option>
												<option>Seine-et-Marne</option>
												<option>Normandie</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="block-recherche">
								<div class="field">
									<label class="label has-text-white">Catégorie</label>
									<div class="control">
										<div class="select is-info is-medium">
											<select class="is-focused">
												<option>Informatique</option>
												<option>Multimédia</option>
												<option>Electroménager</option>
												<option>Téléphonie</option>
												<option>Auto</option>
												<option>Immobilier</option>
												<option>Emploi</option>
											</select>
										</div>
									</div>
								</div>
								<div class="field">
									<label for="code_postal" class="label has-text-white">Code Postal</label>
									<div class="control">
										<input id="code_postal" class="input is-medium width-input" type="text" placeholder="Code postal...">
									</div><br><br>
								</div>
							</div>
							<a class="button  is-link is-outlined has-border-white has-text-white button-search">Rechercher</a><br><br>
						</form>
					</div>
				</section>
			</div>
		</div>
	</div>


	<!-- ANNONCES -->
	<section class="has-background-light">
		<div class="container">
			<div class="columns">
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media ">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="columns">
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="columns">
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="columns">
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="columns">
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-image">
							<figure class="image is-4by3">
								<img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image">
							</figure>
						</div>
						<div class="card-content">
							<div class="media">
								<div class="media-left">
									<figure class="image is-48x48">
										<img class="is-rounded" src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
									</figure>
								</div>
								<div class="media-content">
									<p class="title is-4">John Smith</p>
								</div>
								<div class="media-right">
									<span class="icon">
										<a href="#"><i class="far fa-heart"></i></a>
									</span>
								</div>
							</div>

							<div class="content">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Phasellus nec iaculis mauris.
								</p>
								<time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- PAGINATION -->
			<nav class="pagination" role="navigation" aria-label="pagination">
				<a class="pagination-previous">Previous</a>
				<a class="pagination-next">Next page</a>
				<ul class="pagination-list">
					<li>
						<a class="pagination-link" aria-label="Goto page 1">1</a>
					</li>
					<li>
						<span class="pagination-ellipsis">&hellip;</span>
					</li>
					<li>
						<a class="pagination-link" aria-label="Goto page 45">45</a>
					</li>
					<li>
						<a class="pagination-link is-current" aria-label="Page 46" aria-current="page">46</a>
					</li>
					<li>
						<a class="pagination-link" aria-label="Goto page 47">47</a>
					</li>
					<li>
						<span class="pagination-ellipsis">&hellip;</span>
					</li>
					<li>
						<a class="pagination-link" aria-label="Goto page 86">86</a>
					</li>
				</ul>
			</nav>
		</div>
	</section>

	<footer class="footer">
		<div class="content has-text-centered has-text-white ">
			<p>
				Site développé par Faudel, Amine et Habacuc.
			</p>
			<h4 class="has-text-white">
				Copyright &amp; tous droits reservés.
			</h4>
		</div>
	</footer>

</body>

</html>
<?php
    }
}